module ApplicationHelper

	# Returns the page-title
	def full_title(page_title)
		base_title = 'This is a first app'
		if page_title.empty?
			base_title
		else
			'#{base_title} | #{page_title}'
		end
	end
			
end
